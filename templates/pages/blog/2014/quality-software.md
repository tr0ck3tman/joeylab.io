---
layout: post.html
title: Definition of Quality in Software
summary: Has your boss ever asked you to increase your code quality? How did you do that?
should_publish: yes
published: 2014-02-01T16:43:08.111Z
---
# Definition of Quality in Software

Has your boss ever asked you to increase your code quality? How did you do that?

When mine asked, I was stumped. What does code quality look like? How would I know quality code when I saw it? So I embarked on a journey to first define quality in the context of software development. I came up with a definition from 2 different perspectives because I thought quality would mean different things depending on who you asked.

As a user of the software, quality is the standard of page load, user error count, and consistency as measured against the previous version’s page load, user error count and consistency.

As a developer of the software, quality is the standard of readability, discoverability and leverage of existing code as measured against the previous version’s readability, discoverability and leverage of existing code.

Now that I have a definition to work with, I can now set out on the journey to measure it.