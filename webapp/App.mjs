import MakeObservableList from "./MakeObservableList.mjs"

const pipe = (...fns) => x => fns.reduce((y, f) => f(y), x)
class App {
    constructor(window, socket, user){
        this.window = window
        this.user = user
        this.socket = socket
        this.views = []
        this.subscriptions = {}
        this.messages = MakeObservableList()
        this.socket.on("update", this.update.bind(this))
        this.socket.on("joined", this.joined.bind(this))
    }
    update(message){
        console.log("got message from update: ", message.substr(0, 20))
        if(!message) return
        if(message.length == 0) return
        if(message.indexOf("<html") == -1) return
        window.location.reload(true)
        // const parser = new DOMParser()
        // const doc = parser.parseFromString(message, "text/html")
        // this.window.document.body = doc.body
        // this.window.document.open()
        // this.window.document.write(message)
        // this.window.document.close()
}
    joined(message){
        console.log("Someone just joined", message)
    }
    send(message){
        this.socket.emit("sending message from user", message)
    }
}
export default App